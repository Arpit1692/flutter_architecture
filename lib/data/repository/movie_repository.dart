import 'package:dartz/dartz.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/movie_request.dart';
import 'package:flutter_architecture/data/models/movie_response.dart';
import 'package:flutter_architecture/data/source/movie_datasource.dart';

class MovieRepository {
  MovieDataSource movieDataSource;

  MovieRepository() {
    movieDataSource = MovieSource();
  }

  Future<Either<GenericFailure, MovieResponse>> getMovies(MovieModelReq req) async {
    return await movieDataSource.getMovies(req);
  }
}
