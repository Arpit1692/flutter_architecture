import 'package:dartz/dartz.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/search_request.dart';
import 'package:flutter_architecture/data/models/search_response.dart';
import 'package:flutter_architecture/data/source/search_data_source.dart';

class SearchRepository {
  SearchDataSource searchDataSource;

  SearchRepository() {
    searchDataSource = SearchSource();
  }

  Future<Either<GenericFailure, SearchResponse>> searchMovies(SearchModelReq req) async {
    return await searchDataSource.searchMovies(req);
  }
}
