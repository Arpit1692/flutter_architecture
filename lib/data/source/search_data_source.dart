import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/search_request.dart';
import 'package:flutter_architecture/data/models/search_response.dart';
import 'package:http/http.dart' as http;

abstract class SearchDataSource {
  Future<Either<GenericFailure, SearchResponse>> searchMovies(SearchModelReq req);
}

class SearchSource implements SearchDataSource {
  SearchSource();

  @override
  Future<Either<GenericFailure, SearchResponse>> searchMovies(SearchModelReq req) async {
    final response = await http.get(Uri.parse('https://api.themoviedb.org/3/search/movie?api_key=bbb379137132355d2b32b373571084d9&language=en-US&query=${req.query}&page=1&include_adult=false'));

    if (response.statusCode == 200) {
      return Right(
          SearchResponse.fromJson(json.decode(response.body))
      );
    } else {
      throw Exception('Failed to search movies');
    }
  }
}
