
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SearchEntityReq extends EntityReq {
  final String query;

  SearchEntityReq({
    @required this.query,
  }) : super([query]);
}

abstract class EntityReq extends Equatable {
  final List props;

  EntityReq([this.props = const []]) : super([props]);
}