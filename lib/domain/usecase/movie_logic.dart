import 'package:dartz/dartz.dart';
import 'package:flutter_architecture/core/functions.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/movie_request.dart';
import 'package:flutter_architecture/data/models/movie_response.dart';
import 'package:flutter_architecture/data/repository/movie_repository.dart';
import 'package:flutter_architecture/domain/entity/movie_entity.dart';
import 'package:http/http.dart' as http;

class MovieLogic {
  MovieRepository movierepository;

  MovieLogic() {
    movierepository = MovieRepository();
  }


  Future<Either<GenericFailure, MovieResponse>> getMovie(MovieEntityReq req) async{
    var reqM = MovieModelReq(
      page: req.page
    );
    Either<GenericFailure, MovieResponse> resM = await movierepository.getMovies(reqM);

    if (resM.isLeft()) {
      return Left(getLeftOfEither(resM));
    } else {
      var resMright = (getRightOfEither(resM) as MovieResponse);

      //var res = MovieResponse(resMright.uuid);

      return Right(resMright);
    }
  }
}
