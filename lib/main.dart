import 'package:flutter/material.dart';
import 'package:flutter_architecture/presentation/ui/detail_page.dart';
import 'package:flutter_architecture/presentation/ui/homepage.dart';
import 'package:flutter_architecture/presentation/ui/search_page.dart';
import 'package:flutter_architecture/services/service_locator.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setUpLocators();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: MyHomePage.id,
      routes: {
        MyHomePage.id: (context) => MyHomePage(),
        DetailsPage.id: (context) => DetailsPage(),
        SearchPage.id: (context) => SearchPage(),
      },
    );
  }
}