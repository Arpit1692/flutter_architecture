import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/movie_response.dart';
import 'package:flutter_architecture/domain/entity/movie_entity.dart';
import 'package:flutter_architecture/domain/usecase/movie_logic.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieLogic movieLogic = MovieLogic();



  @override
  MovieState get initialState => Initial();

  MovieBloc({MovieState initialState}) : super(initialState);

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async*{
    if(event is MovieRequest){
      final failureOrSignInGoogleEntity = await movieLogic.getMovie(
          MovieEntityReq(
           page: event.page,
          ),
      );
      yield* _eitherFailureOrSuccess(failureOrSignInGoogleEntity);
    }
  }

  Stream<MovieState> _eitherFailureOrSuccess(
      Either<GenericFailure, MovieResponse> failureOrSuccess,
      ) async* {
    yield failureOrSuccess.fold(
          (failure) => Error(
        failure: failure,
      ),
          (movieResponse) => Response(
        movieResponse,
      ),
    );
  }
}

@immutable
abstract class MovieEvent extends Equatable {
  MovieEvent([List props = const <dynamic>[]]) : super(props);
}



class MovieRequest extends MovieEvent {
  final int page;

  MovieRequest({
    @required this.page,
  }) : super([page]);
}

@immutable
abstract class MovieState extends Equatable {
  MovieState([List props = const <dynamic>[]]) : super(props);
}

class Response extends MovieState {
  final MovieResponse res;

  Response(this.res) : super([res]);
}

class Initial extends MovieState {}

class Error extends MovieState {
  final GenericFailure failure;

  Error({@required this.failure}) : super([failure]);
}
