import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_architecture/core/generic_failures.dart';
import 'package:flutter_architecture/data/models/search_response.dart';
import 'package:flutter_architecture/domain/entity/search_entity.dart';
import 'package:flutter_architecture/domain/usecase/search_logic.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchLogic searchLogic = SearchLogic();



  @override
  SearchState get initialState => Initial();

  SearchBloc({SearchState initialState}) : super(initialState);

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async*{
    if(event is SearchRequest){
      final failureorSuccess = await searchLogic.searchMovie(
          SearchEntityReq(
           query: event.query,
          ),
      );
      yield* _eitherFailureOrSuccess(failureorSuccess);
    }
  }

  Stream<SearchState> _eitherFailureOrSuccess(
      Either<GenericFailure, SearchResponse> failureOrSuccess,
      ) async* {
    yield failureOrSuccess.fold(
          (failure) => Error(
        failure: failure,
      ),
          (searchResponse) => Response(
        searchResponse,
      ),
    );
  }
}

@immutable
abstract class SearchEvent extends Equatable {
  SearchEvent([List props = const <dynamic>[]]) : super(props);
}



class SearchRequest extends SearchEvent {
  final String query;

  SearchRequest({
    @required this.query,
  }) : super([query]);
}

@immutable
abstract class SearchState extends Equatable {
  SearchState([List props = const <dynamic>[]]) : super(props);
}

class Response extends SearchState {
  final SearchResponse res;

  Response(this.res) : super([res]);
}

class Initial extends SearchState {}

class Error extends SearchState {
  final GenericFailure failure;

  Error({@required this.failure}) : super([failure]);
}
