
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_architecture/presentation/bloc/movie_bloc.dart';
import 'package:flutter_architecture/presentation/ui/detail_page.dart';
import 'package:flutter_architecture/presentation/ui/search_page.dart';
import 'package:flutter_architecture/services/service_locator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MyHomePage extends StatefulWidget {
  static const String id = 'home_page';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  int count = 1;
  bool isLoading = false;
  final MovieBloc movieBloc = sl<MovieBloc>();
  List<dynamic> res = [];

  @override
  void initState() {
    super.initState();
    dispatchMovies();
  }
  
  

  dispatchMovies(){
   movieBloc.add(MovieRequest(page: count));
  }

  Future _loadData() async {
    await new Future.delayed(new Duration(seconds: 2)).then((value) =>
        movieBloc.add(MovieRequest(page: count++)));

    setState(() {
      res.clear();
      isLoading = false;
    });

  }





@override
  void dispose() {
   super.dispose();
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
    backgroundColor: Colors.white,
    appBar: AppBar(
     centerTitle: true,
     backgroundColor: Colors.white,
     title: Text("The Internet Movie Database",style: TextStyle(color:Colors.black),),
    ),
    floatingActionButton: FloatingActionButton(
      splashColor: Colors.white,
      child: Icon(Icons.search),
      onPressed: (){
       Navigator.of(context).pushNamed(SearchPage.id);
      },
    ),
    body: SafeArea(
    child: Container(
     padding: EdgeInsets.all(10),
     child: Center(
      child: blocListener(context),
   )
    )));
  }

  Widget blocListener(BuildContext context) {
    return BlocListener(
      bloc: movieBloc,
      listener: (BuildContext context, MovieState state) {
        if (state is Error) {
          print("Error");
        }
      },
      child: blocProvider(context),
    );
  }

  BlocProvider<MovieBloc> blocProvider(BuildContext context) {
    return BlocProvider(
      create: (context) => movieBloc,
      child: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          Widget display = CircularProgressIndicator();
          if (state is Initial) {
            display =  Center(
             child:  CircularProgressIndicator()
            );
          }else if(state is Response){
            res.addAll(state.res.result);
           // res = state.res.result;
            //res.add(state.res.result);
            if(res == null){
              display =  Center(
                  child:  CircularProgressIndicator()
              );
            }else {
              display = _buildPaginatedListView(res);
            }
          }
          return display;
        },
      ),
    );
  }

  Widget _buildPaginatedListView(List result) {
    return RefreshConfiguration(
        footerTriggerDistance: 15,
        dragSpeedRatio: 0.91,
        headerBuilder: () => MaterialClassicHeader(),
        footerBuilder: () => ClassicFooter(),
        enableLoadingWhenNoData: false,
        enableRefreshVibrate: false,
        enableLoadMoreVibrate: false,
        shouldFooterFollowWhenNotFull: (state) {
          // If you want load more with noMoreData state ,may be you should return false
          return false;
        },
        child: Column(
      children: <Widget>[
        Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading && scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {// start loading data
                  setState(() {
                    isLoading = true;
                  });
                  _loadData();
                }
                return isLoading;
              },
              child: MovieAdapter(result),
            )
        ),
        Container(
          height: isLoading ? 50.0 : 0,
          color: Colors.white70,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    ));
  }


}

MovieAdapter(List result) {
  return ListView.builder(
   itemCount: result.length,
   itemBuilder: (BuildContext context, int index) {
    return InkWell(
      onTap: (){
        Navigator.push(context,
            MaterialPageRoute(
              builder: (context) => DetailsPage(data: result[index]),
            ));
      },
      child: Container(
        height: 110, width: double.infinity,
        padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Card(
                      margin: EdgeInsets.all(0), elevation: 0,
                      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(8),),
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Image.network('http://image.tmdb.org/t/p/w185${result[index]['poster_path']}', height: 100, width: 100, fit: BoxFit.cover)
                  ),
                  Container(width: 10),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(flex: 1,child: Text(result[index]['title'],overflow: TextOverflow.ellipsis,maxLines: 2, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                                ),Text("Rating: "+result[index]['vote_average'].toString(), style: TextStyle(color: Colors.black)),
                          ],
                        ),
                        Spacer(),
                        Text(result[index]['overview'], maxLines:3,overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),


                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(height: 10),
            Divider(height: 0)
          ],
        ),
      ),
    );
   }
  );
}




