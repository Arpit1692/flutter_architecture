
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_architecture/presentation/bloc/search_bloc.dart';
import 'package:flutter_architecture/presentation/ui/detail_page.dart';
import 'package:flutter_architecture/services/service_locator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchPage extends StatefulWidget {
  static const String id = 'search_page';

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> with TickerProviderStateMixin{
  final SearchBloc searchBloc = sl<SearchBloc>();
  Widget appBarTitle = new Text("The Internet Movie Database",style: TextStyle(color:Colors.black),);
  Icon actionIcon = new Icon(Icons.search);
  TextEditingController queryController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }



  /*dispatchMovies(){
    searchBloc.add(SearchRequest(query: "Ghosts"));
  }
*/
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          //centerTitle: true,
          leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
           child: Icon(Icons.arrow_back,color: Colors.black,),
          ),
          backgroundColor: Colors.white,
          title: Center(
           child: appBarTitle,
          ),
            actions: <Widget>[
              new IconButton(icon: actionIcon,color: Colors.black,onPressed:(){
                setState(() {
                  if ( this.actionIcon.icon == Icons.search){
                    this.actionIcon = new Icon(Icons.close);
                    this.appBarTitle = new Container(
                      margin: EdgeInsets.only(bottom: 20),
                     child: TextField(
                       controller: queryController,
                       textInputAction: TextInputAction.done,
                       onChanged: (value){
                         searchBloc.add(SearchRequest(query: queryController.text));
                       },
                       onSubmitted: (term){
                         searchBloc.add(SearchRequest(query: queryController.text));
                       },
                       style: new TextStyle(
                         color: Colors.black,

                       ),
                       decoration: new InputDecoration(
                           contentPadding: EdgeInsets.only(top: 20),
                           hintText: "Type to Search for a movie",
                           hintStyle: new TextStyle(color: Colors.black)
                       ),
                     ),
                    );}
                  else {
                    this.actionIcon = new Icon(Icons.search);
                    this.appBarTitle = new Text("The Internet Movie Database",style: TextStyle(color: Colors.black),);
                  }


                });
              } ,),]
        ),
        body: SafeArea(
            child: Container(
                padding: EdgeInsets.all(10),
                child: Center(
                  child: blocListener(context),
                )
            )));
  }

  Widget blocListener(BuildContext context) {
    return BlocListener(
      bloc: searchBloc,
      listener: (BuildContext context, SearchState state) {
        if (state is Error) {

        }
      },
      child: blocProvider(context),
    );
  }

  BlocProvider<SearchBloc> blocProvider(BuildContext context) {
    return BlocProvider(
      create: (context) => searchBloc,
      child: BlocBuilder<SearchBloc, SearchState>(
        builder: (context, state) {
          Widget display = Center(
              child: Text("No Data to Show",style: TextStyle(color: Colors.black),)
          );
          if (state is Initial) {
            display = CircularProgressIndicator();
            Future.delayed(const Duration(milliseconds: 3000), () {

            });
            display =  Container(color: Colors.white,
            child: Center(
              child: Text("No Data to Show",style: TextStyle(color: Colors.black),)
            ),);
          }else if(state is Response){
            print(state.res.result);
            display = display = _buildPaginatedListView(state.res.result);
          }
          return display;
        },
      ),
    );
  }

  Widget _buildPaginatedListView(List result) {
    return Column(
      children: <Widget>[
        Expanded(
          child: searchAdapter(result),
        ),
      ],
    );
  }
}

searchAdapter(List result) {
  return ListView.builder(
      itemCount: result.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: (){
            Navigator.push(context,
                MaterialPageRoute(
                  builder: (context) => DetailsPage(data: result[index]),
                ));
          },
          child: Container(
            height: 110, width: double.infinity,
            padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Card(
                          margin: EdgeInsets.all(0), elevation: 0,
                          shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(8),),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Image.network('http://image.tmdb.org/t/p/w185${result[index]['poster_path']}', height: 100, width: 100, fit: BoxFit.cover)
                      ),
                      Container(width: 10),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(flex: 1,child: Text(result[index]['title'],overflow: TextOverflow.ellipsis,maxLines: 2, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                                ),Text("Rating: "+result[index]['vote_average'].toString(), style: TextStyle(color: Colors.black)),
                              ],
                            ),
                            Spacer(),
                            Text(result[index]['overview'], maxLines:3,overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),


                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(height: 10),
                Divider(height: 0)
              ],
            ),
          ),
        );
      }
  );
}



