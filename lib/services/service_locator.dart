import 'package:flutter_architecture/presentation/bloc/movie_bloc.dart';
import 'package:flutter_architecture/presentation/bloc/search_bloc.dart';
import 'package:get_it/get_it.dart';


GetIt sl = GetIt.instance;

void setUpLocators() {

  // Register Blocs
  sl.registerLazySingleton<MovieBloc>(() => MovieBloc());
  sl.registerLazySingleton<SearchBloc>(() => SearchBloc());

}
